#!/usr/bin/env python3

from argparse import ArgumentParser, FileType, RawDescriptionHelpFormatter
import os
import queue
import sys
import json
import re


import sounddevice as sd
from vosk import KaldiRecognizer, Model

q = queue.Queue()

models = {
    "en-us": os.path.join(os.path.dirname(__file__), "models/vosk-model-small-en-us-0.15"),
    # "ml": os.path.join(os.path.dirname(__file__), "models/vosk-model-malayalam-bigram"),
    "ml": os.path.join(os.path.dirname(__file__), "models/sbpe-4-model"),

}

    

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text


def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    q.put(bytes(indata))


parser = ArgumentParser(add_help=False)
parser.add_argument("-l", "--list-devices", action="store_true", help="show list of audio devices and exit")
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = ArgumentParser(
    description=__doc__, formatter_class=RawDescriptionHelpFormatter, parents=[parser]
)
parser.add_argument("-f", "--filename", type=str, metavar="FILENAME", help="audio file to store recording to")
parser.add_argument("-d", "--device", type=int_or_str, help="input device (numeric ID or substring)")
parser.add_argument("-r", "--samplerate", type=int, help="sampling rate")
parser.add_argument(
    "-m", "--model", default="ml", type=str, help="language model; e.g. en-us, fr, nl; default is en-us"
)
parser.add_argument(
        "-o",
        "--outfile",
        type=FileType("w"),
        nargs="?",
        default=sys.stdout,
        help="File to write, if empty, stdout is used",
    )
args = parser.parse_args(remaining)

try:
    if args.samplerate is None:
        device_info = sd.query_devices(args.device, "input")
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info["default_samplerate"])

    model = Model(lang=args.model, model_path=models.get(args.model))

    if args.filename:
        dump_fn = open(args.filename, "wb")
    else:
        dump_fn = None

    with sd.RawInputStream(
        samplerate=args.samplerate, blocksize=8000, device=args.device, dtype="int16", channels=1, callback=callback
    ):
        # print("#" * 80)
        # print("Press Ctrl+C to stop the recording")
        # print("#" * 80)

        rec = KaldiRecognizer(model, args.samplerate)
        while True:
            data = q.get()
            if rec.AcceptWaveform(data):
                recognized_text_obj = json.loads(rec.Result())
                recognized_text = recognized_text_obj['text']
                output_text = re.sub(r'\+ ', '', recognized_text)
                if(output_text):
                    args.outfile.write(output_text+".\n")
                sys.stdout.flush()
            if dump_fn is not None:
                dump_fn.write(data)

except KeyboardInterrupt:
    print("\nDone")
    parser.exit(0)
except Exception as e:
    parser.exit(type(e).__name__ + ": " + str(e))
