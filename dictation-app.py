import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
import subprocess
import threading

class SimpleDictationApp:
    def __init__(self):
        self.window = Gtk.Window()
        self.window.set_title("പറയൂ...എഴുതാം: Live Dictation")
        self.window.set_default_size(400, 300)
        self.window.connect("destroy", Gtk.main_quit)

        self.textview = Gtk.TextView()
        self.textview.set_wrap_mode(Gtk.WrapMode.WORD)

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_hexpand(True)
        scrolled_window.set_vexpand(True)
        scrolled_window.add(self.textview)

        self.clear_button = Gtk.Button(label="Clear Text")
        self.clear_button.connect("clicked", self.on_clear_button_clicked)

        self.start_button = Gtk.Button(label="START Dictation")
        self.start_button.connect("clicked", self.on_start_button_clicked)

        self.end_button = Gtk.Button(label="END Dictation")
        self.end_button.connect("clicked", self.on_end_button_clicked)

        self.download_button = Gtk.Button(label="Download as output.txt")
        self.download_button.connect("clicked", self.on_download_button_clicked)

        vbox = Gtk.VBox()
        vbox.pack_start(scrolled_window, True, True, 0)
        vbox.pack_start(self.clear_button, False, False, 5)
        vbox.pack_start(self.start_button, False, False, 5)
        vbox.pack_start(self.end_button, False, False, 5)
        vbox.pack_start(self.download_button, False, False, 5)

        self.window.add(vbox)
        self.window.show_all()

        self.process = None
        self.output_thread = None

    def on_clear_button_clicked(self, button):
        self.textview.get_buffer().set_text("")

    def on_start_button_clicked(self, button):
        if self.process is None:
            self.process = subprocess.Popen(['python', 'asr.py'], stdout=subprocess.PIPE, text=True)
            self.output_thread = threading.Thread(target=self.read_output)
            self.output_thread.start()
            self.start_button.set_sensitive(False)
            self.end_button.set_sensitive(True)

    def on_end_button_clicked(self, button):
        if self.process is not None:
            self.process.terminate()
            self.output_thread.join()
            self.process = None
            self.start_button.set_sensitive(True)
            self.end_button.set_sensitive(False)

    def read_output(self):
        while self.process.poll() is None:
            line = self.process.stdout.readline()
            if line:
                GLib.idle_add(self.update_text, line)

    def update_text(self, line):
        buffer = self.textview.get_buffer()
        buffer.insert_at_cursor(line, -1)

    def on_download_button_clicked(self, button):
        buffer = self.textview.get_buffer()
        start_iter = buffer.get_start_iter()
        end_iter = buffer.get_end_iter()
        text = buffer.get_text(start_iter, end_iter, False)

        with open("output.txt", "w") as file:
            file.write(text)

        dialog = Gtk.MessageDialog(
            self.window,
            Gtk.DialogFlags.MODAL,
            Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK,
            "Text has been saved to output.txt"
        )
        dialog.run()
        dialog.destroy()

def main():
    app = SimpleDictationApp()
    Gtk.main()

if __name__ == "__main__":
    main()
 
