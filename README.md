# Simple Dictation Desktop App

Convert dictated speech to text with ease using this simple desktop application built using Python and GTK. The application features a text area where you can start and stop dictation, view the transcribed text, clear the content, and even save it as an `output.txt` file.

## Features

- Convert dictated speech to text.
- Clear the text area to start fresh.
- Save transcribed text as an `output.txt` file for future reference.

## Prerequisites

- Python 3.x
- GTK library (PyGObject)
- Additional requirements for dictation functionality:
  - vosk
  - sounddevice

## Getting Started

1. Clone this repository to your local machine.

2. Create and activate a virtual environment (recommended) to manage dependencies:

    ```bash
    python3 -m venv venv
    source venv/bin/activate  # On Windows, use: venv\Scripts\activate
    ```

3. Install the required dependencies using the following commands:

    ```bash
    pip install -r requirements.txt
    ```

4. Run the application using:

    ```bash
    python dictation_app.py
    ```

5. Interact with the application using the provided buttons:
   - "START Dictation": Begin transcribing speech into the text area.
   - "END Dictation": Stop transcribing speech and display the transcribed text.
   - "Clear Text": Remove the transcribed content from the text area.
   - "Download as output.txt": Save the transcribed text as an `output.txt` file.

## Notes

- The application uses vosk and sounddevice to convert speech to text.
- Make sure the `asr.py` script is present in the same directory as the `dictation_app.py` script.
- The application utilizes GTK for the user interface and subprocess for running the `asr.py` script.
- By default, the desktop app (`dictation_app.py`) calls `asr.py` to convert speech to text using a Malayalam ASR model built using vosk present in the `models` directory.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## TODO

- Add option to record and transcribe later.
- Add option to transcribe a file.
- Add option to choose the model.

## FAQ on the model

1. What is a model?

The model refers to a weighted finite state trasducer (WFST) graph composed of an acoustic model, language model and the proncuniation dictionary. It is created using the [Kaldi](https://kaldi-asr.org/) toolkit. The dictation app uses the [Vosk](https://alphacephei.com/vosk/) library to decode the speech using this model

2. How was the model in this repo created?

The specific model in this repo was created using the training scripts [here](https://gitlab.com/kavyamanohar/ml-subword-asr). It uses subword token based [language models](https://gitlab.com/kavyamanohar/subword-segmentation-models) and lexicons. 

3. How to use a different model?

You can use any models in any language compatiable with Vosk, available [here](https://alphacephei.com/vosk/models).



